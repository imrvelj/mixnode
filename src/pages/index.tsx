import { FC, useCallback } from 'react'
import { useSetAtom } from 'jotai'

import { replaceQueueAtom, startPlayingAtom } from '@/atoms/player'
import { useFetchDiscover } from '@/hooks/useFetchDiscover'
import { CloudcastList } from '@/components/CloudcastList'

const IndexPage: FC = () => {
  const { data } = useFetchDiscover('popular')
  const replaceQueue = useSetAtom(replaceQueueAtom)
  const startPlaying = useSetAtom(startPlayingAtom)

  const setPlaying = useCallback(
    async (track: string) => {
      await replaceQueue([track])
      startPlaying()
    },
    [replaceQueue, startPlaying],
  )

  return (
    <div>
      {data ? (
        <CloudcastList
          cloudcasts={data}
          onClick={track => {
            setPlaying(track).catch(() => null)
          }}
        />
      ) : null}
    </div>
  )
}

export default IndexPage
