import 'normalize.css'
import { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'

import { App } from '@/components/app/App'

import '@/styles/index.scss'

function init() {
  const root = createRoot(document.getElementById('main'))

  root.render(
    <StrictMode>
      <App />
    </StrictMode>,
  )
}

init()
