export function createScript(src: string) {
  const script = document.createElement('script')
  script.async = true
  script.src = src
  script.type = 'text/javascript'
  return script
}
