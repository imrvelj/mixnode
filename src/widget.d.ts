interface FooterWidget {
  load: (song: string, startPlaying?: boolean) => Promise<null>
  play: () => void
  pause: () => void
  events: {
    play: {
      on: (listener: () => void) => void
      off: (listener: () => void) => void
    }
    pause: {
      on: (listener: () => void) => void
      off: (listener: () => void) => void
    }
    progress: {
      on: (listener: (position: number, duration: number) => void) => void
      off: (listener: (position: number, duration: number) => void) => void
    }
  }
}

declare const Mixcloud: {
  PlayerWidget: (HTMLIFrameElement) => void
  FooterWidget: (string, unknown) => Promise<FooterWidget>
}
