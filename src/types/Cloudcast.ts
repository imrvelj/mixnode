interface CloudcastTag {
  key: string
  url: string
  name: string
}

interface Pictures {
  small?: string
  thumbnail?: string
  medium_mobile?: string
  medium?: string
  large?: string
  '320wx320h'?: string
  extra_large?: string
  '640wx640h'?: string
  '768wx768h'?: string
  '1024wx1024h'?: string
}

export interface Cloudcast {
  slug: string
  key: string
  url: string
  name: string
  tags: CloudcastTag[]
  created_time: string
  updated_time: string
  play_count: number
  favorite_count: number
  comment_count: number
  listener_count: number
  repost_count: number
  pictures: Pictures
  user: {
    key: string
    url: string
    name: string
    username: string
    pictures: Pictures
  }
  audio_length: number
}
