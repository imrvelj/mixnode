import { useQuery } from 'react-query'

import { Cloudcast } from '@/types/Cloudcast'

interface DiscoverResponse {
  data: Cloudcast[]
}

export const useFetchDiscover = (
  tag: string,
  type: 'popular' | 'latest' = 'popular',
) =>
  useQuery(`discover-${tag}-${type}`, () =>
    fetch(`https://api.mixcloud.com/discover/${tag}/${type}?limit=50`)
      .then(res => res.json() as Promise<DiscoverResponse>)
      .then(r => r.data),
  )
