import { FC, ReactNode } from 'react'

import { Header } from '@/components/Header'
import { Player } from '@/components/Player'

export const Layout: FC<{ children?: ReactNode }> = ({ children }) => (
  <div className="h-screen flex flex-col">
    <Header />
    <div className="overflow-auto">{children}</div>
    <Player />
  </div>
)
