import { lazy } from 'react'
import { createHashRouter } from 'react-router-dom'

const Index = lazy(() => import('@/pages/index'))

export const router = createHashRouter([
  {
    path: '/',
    element: <Index />,
  },
])
