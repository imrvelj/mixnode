import { FC } from 'react'
import { RouterProvider } from 'react-router-dom'

import { Providers } from './Providers'
import { Layout } from './Layout'
import { router } from './Router'

export const App: FC = () => (
  <Providers>
    <Layout>
      <RouterProvider router={router} />
    </Layout>
  </Providers>
)
