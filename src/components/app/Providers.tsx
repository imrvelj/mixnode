import { FC, Suspense, ReactNode } from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'

const queryClient = new QueryClient()

export const Providers: FC<{ children: ReactNode }> = ({ children }) => (
  <QueryClientProvider client={queryClient}>
    <Suspense fallback={<div className="h-screen">Loading...</div>}>
      {children}
    </Suspense>
  </QueryClientProvider>
)
