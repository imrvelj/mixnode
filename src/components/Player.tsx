import { useEffect, useRef } from 'react'
import { useAtomValue, useSetAtom } from 'jotai'

import {
  widgetAtom,
  playingTrackAtom,
  setWidgetAtom,
  startPlayingAtom,
  stopPlayingAtom,
  isPlayingAtom,
} from '@/atoms/player'
import { createScript } from '@/utils/script'

import { Button } from './ui/Button'
import { Seekbar } from './Seekbar'

const PlayButton = () => {
  const isPlaying = useAtomValue(isPlayingAtom)

  const startPlaying = useSetAtom(startPlayingAtom)
  const stopPlaying = useSetAtom(stopPlayingAtom)

  return isPlaying ? (
    <Button type="button" onClick={() => startPlaying()}>
      Play
    </Button>
  ) : (
    <Button type="button" onClick={() => stopPlaying()}>
      Pause
    </Button>
  )
}

const SongDisplay = () => {
  const song = useAtomValue(playingTrackAtom)
  return <span className="ml-2 text-sm">{song}</span>
}

export const Player = () => {
  const baseScriptRef = useRef<HTMLScriptElement>(null)
  const widgetScriptRef = useRef<HTMLScriptElement>(null)

  const widget = useAtomValue(widgetAtom)

  const setWidget = useSetAtom(setWidgetAtom)

  /*
   * This mess is required even though Mixcloud's docs say otherwise
   * (we need to load both the widgetApi.js and the footerWidgetApi.js files)
   */
  useEffect(() => {
    baseScriptRef.current = createScript(
      '//widget.mixcloud.com/media/js/widgetApi.js',
    )
    widgetScriptRef.current = createScript(
      '//widget.mixcloud.com/media/js/footerWidgetApi.js',
    )

    baseScriptRef.current.onload = () => {
      document.body.appendChild(widgetScriptRef.current)
    }

    widgetScriptRef.current.onload = async () => {
      const w = await Mixcloud.FooterWidget('/spartacus/party-time/', {
        disablePushstate: true,
        disableUnloadWarning: true,
      })
      await setWidget(w)
    }

    document.body.appendChild(baseScriptRef.current)

    return () => {
      if (baseScriptRef) {
        baseScriptRef.current.remove()
      }
      if (widgetScriptRef) {
        widgetScriptRef.current.remove()
      }
      baseScriptRef.current = null
      widgetScriptRef.current = null
    }
  }, [setWidget])

  if (!widget) {
    return null
  }

  return (
    <div className="h-16 mt-auto flex-shrink-0 flex items-center bg-slate-800 text-white">
      <PlayButton />
      <Seekbar />
      <SongDisplay />
    </div>
  )
}
