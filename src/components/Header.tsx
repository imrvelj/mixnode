import { FC } from 'react'

interface Props {
  className?: string
}

export const Header: FC<Props> = ({ className = '' }) => (
  <header
    className={`h-16 px-4 flex-shrink-0 flex items-center text-white bg-slate-900 ${className}`}
  >
    Mixnode
  </header>
)
