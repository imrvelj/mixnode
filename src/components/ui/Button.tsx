import { FC, forwardRef, HTMLProps, PropsWithChildren, useMemo } from 'react'

interface Props extends HTMLProps<HTMLButtonElement> {
  type?: 'button' | 'submit' | 'reset'
  variant?: 'primary' | 'sharp' | 'icon'
}
export const Button: FC<PropsWithChildren<Props>> = forwardRef(
  (
    {
      type = 'button',
      variant = 'primary',
      className = '',
      children = 'Click',
      ...props
    },
    ref,
  ) => {
    const variantClassName = useMemo(() => {
      switch (variant) {
        case 'sharp':
          return 'px-4 text-xs rounded-sm uppercase'
        case 'icon':
          return 'w-8 text-xs rounded-full uppercase'
        default:
          return 'px-6 rounded-3xl'
      }
    }, [variant])

    return (
      <button
        // eslint-disable-next-line react/button-has-type
        type={type}
        ref={ref}
        className={`h-8 inline-flex bg-primary text-white items-center justify-center transition-all text-xs font-semibold cursor-pointer outline-none ${variantClassName} ${className}`}
        data-element="button"
        {...props}
      >
        {children}
      </button>
    )
  },
)
