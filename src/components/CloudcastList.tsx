import { FC } from 'react'

import { Cloudcast } from '@/types/Cloudcast'

interface Props {
  cloudcasts: Cloudcast[]
  onClick?: (show: string) => void
}
export const CloudcastList: FC<Props> = ({
  cloudcasts,
  onClick = () => null,
}) => (
  <ul className="grid grid-cols-4">
    {cloudcasts.map(({ key, name, pictures }) => (
      <li
        role="none"
        key={key}
        className="h-16 flex"
        onClick={() => onClick(key)}
      >
        <img src={pictures.thumbnail} alt={name} />
        <span>{name}</span>
      </li>
    ))}
  </ul>
)
