import { FC } from 'react'
import { useAtomValue } from 'jotai'

import { progressAtom } from '@/atoms/player'

function formatTime(seconds: number) {
  const hours = Math.floor(seconds / 3600)
  const minutes = Math.floor((seconds % 3600) / 60)
  const remainingSeconds = Math.floor(seconds % 60)

  return `${hours > 0 ? `${hours}:` : ''}${minutes
    .toString()
    .padStart(2, '0')}:${remainingSeconds.toString().padStart(2, '0')}`
}

export const Seekbar: FC = () => {
  const [position, duration] = useAtomValue(progressAtom)

  return (
    <div className="flex-grow flex items-center">
      <span className="mr-2 text-sm">{formatTime(position)}</span>
      <progress max={duration} value={position} className="w-full" />
      <span className="ml-2 text-sm">{formatTime(duration)}</span>
    </div>
  )
}
