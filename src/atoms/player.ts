import { atom } from 'jotai'
import { atomWithStorage } from 'jotai/utils'
import { isPlainObject } from 'react-query/types/core/utils'

export const widgetAtom = atom<FooterWidget | false>(false)
export const queueAtom = atomWithStorage('queue', ['spartacus/party-time'])

export const positionInQueueAtom = atom(0)
export const playingTrackAtom = atom(
  get => get(queueAtom)[get(positionInQueueAtom)],
)

export const isPlayingAtom = atom(false)
export const progressAtom = atom([0, 0])

// we need this in order for proper progress listening to work
const progressListenerAtom = atom<{
  fn: (position: number, duration: number) => void | null
}>({ fn: null })

const setProgressListenerAtom = atom(null, (get, set) => {
  const widget = get(widgetAtom)
  const progressListener = get(progressListenerAtom)

  if (!widget) {
    return
  }

  if (progressListener.fn) {
    widget.events.progress.off(progressListener.fn)
  }

  set(progressListenerAtom, {
    fn: (positon, duration) => {
      set(progressAtom, [positon, duration])
    },
  })

  widget.events.progress.on(progressListener.fn)
})

export const setWidgetAtom = atom(
  null,
  async (get, set, value: FooterWidget) => {
    const playingTrack = get(playingTrackAtom)
    await value.load(playingTrack)

    set(widgetAtom, value)
  },
)

export const replaceQueueAtom = atom(
  null,
  async (get, set, value: string[]) => {
    const widget = get(widgetAtom)
    set(queueAtom, value)
    set(positionInQueueAtom, 0)

    if (widget) {
      await widget.load(value[0])
    }
  },
)

export const startPlayingAtom = atom(null, (get, set) => {
  const widget = get(widgetAtom)

  if (!widget) {
    return
  }

  widget.play()
  set(setProgressListenerAtom)
  set(isPlayingAtom, true)
})

export const stopPlayingAtom = atom(null, (get, set) => {
  const widget = get(widgetAtom)
  if (widget) {
    widget.pause()
    set(isPlayingAtom, true)
  }
})
