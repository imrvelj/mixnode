import { join } from 'path'

import installExtension, {
  REACT_DEVELOPER_TOOLS,
} from 'electron-devtools-installer'
// eslint-disable-next-line import/no-extraneous-dependencies
import { app, BrowserWindow, ipcMain } from 'electron'
import fetch from 'node-fetch'

let mainWindow: BrowserWindow | null
let authWindow: BrowserWindow | null
const url = process.env.VITE_DEV_SERVER_URL

function createWindow() {
  mainWindow = new BrowserWindow({
    webPreferences: {
      contextIsolation: false,
      nodeIntegration: true,
    },
  })

  if (app.isPackaged) {
    mainWindow.loadFile(join(__dirname, '../index.html')).catch(() => null)
  } else {
    installExtension(REACT_DEVELOPER_TOOLS).catch(() => null)
    mainWindow.loadURL(url).catch(() => null)
  }
}

app.on('window-all-closed', () => {
  mainWindow = null
})

ipcMain.on('user-auth', () => {
  const clientId = process.env.MIXCLOUD_CLIENT_ID || null

  authWindow = new BrowserWindow({
    width: 500,
    height: 720,
    parent: mainWindow,
    modal: true,
    autoHideMenuBar: true,
    webPreferences: {
      webSecurity: false,
      nodeIntegration: true,
    },
  })

  authWindow
    .loadURL(
      `https://www.mixcloud.com/oauth/authorize?client_id=${clientId}&redirect_uri=${url}`,
    )
    .catch(() => null)

  authWindow.on('closed', () => {
    authWindow = null
  })

  authWindow.webContents.on('will-redirect', (e, newUrl) => {
    const redirectUrl = authWindow.webContents
      .getURL()
      .split('redirect_uri=')[1]
    const code = newUrl.split('code=')[1]
    const authUrl = `https://mixcloud.com/oauth/access_token?client_id=${clientId}&redirect_uri=${redirectUrl}&client_secret=${process.env.MIXNODE_SECRET}&code=${code}`

    fetch(authUrl).then(res => {
      mainWindow.webContents.send('user-log-in', res.json())
      authWindow.close()
    })
  })
})

app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required')

app
  .whenReady()
  .then(createWindow)
  .catch(() => {
    throw new Error('App failed to start.')
  })
