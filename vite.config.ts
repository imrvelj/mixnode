import path from 'path'
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import checker from 'vite-plugin-checker'
import electron from 'vite-plugin-electron'

export default defineConfig({
  plugins: [
    react(),
    checker({ typescript: true }),
    electron({
      entry: 'electron/main.ts',
    }),
  ],
  envPrefix: 'APP_',
  build: {
    sourcemap: true,
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
})
