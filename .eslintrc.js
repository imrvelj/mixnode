const IS_PROD = process.env.NODE_ENV === "production";

module.exports = {
  parser: "@typescript-eslint/parser",
  extends: [
    "eslint:recommended",
    "airbnb",
    "airbnb/hooks",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "prettier",
  ],
  plugins: ["prettier"],
  env: {
    es6: true,
    node: true,
    browser: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
    tsconfigRootDir: __dirname,
    project: ["./tsconfig.json"],
  },
  rules: {
    semi: ["warn", "never"],
    quotes: [
      1,
      "single",
      {
        allowTemplateLiterals: true,
      },
    ],
    strict: [2, "never"],
    curly: [2, "all"],
    camelcase: 0,
    "no-console": 1,
    "no-debugger": IS_PROD ? "error" : "warn",
    "no-param-reassign": 0,
    "arrow-parens": [1, "as-needed"],
    "arrow-spacing": "warn",
    "arrow-body-style": 1,
    "no-use-before-define": 0,
    "no-unused-vars": 0,
    "no-delete-var": "error",
    "no-whitespace-before-property": "warn",
    "no-restricted-syntax": 0,
    "no-confusing-arrow": 0,
    "react/require-default-props": 0,
    "react/prop-types": "off",
    "react/display-name": "off",
    "react/react-in-jsx-scope": "off",
    "react/jsx-filename-extension": ["error", { extensions: [".jsx", ".tsx"] }],
    "react/no-array-index-key": 0,
    "react/jsx-props-no-spreading": 0,
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "error",
    "react/function-component-definition": 0,
    "prettier/prettier": 1,
    "import/extensions": 0,
    "import/prefer-default-export": 0,
    "import/order": [
      1,
      {
        groups: [
          "builtin",
          "external",
          "internal",
          "parent",
          "sibling",
          "index",
        ],
        "newlines-between": "always",
      },
    ],
    "@typescript-eslint/switch-exhaustiveness-check": "error",
    "@typescript-eslint/member-delimiter-style": [
      "warn",
      {
        multiline: {
          delimiter: "none",
          requireLast: false,
        },
        singleline: {
          delimiter: "semi",
          requireLast: false,
        },
      },
    ],
  },
  settings: {
    react: {
      version: "detect",
    },
    "import/parsers": {
      "@typescript-eslint/parser": [".ts", ".tsx"],
    },
    "import/resolver": {
      typescript: {
        alwaysTryTypes: true,
      },
    },
  },
};
